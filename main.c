#define _POSIX_C_SOURCE 200809L //To get getopt() in spite of -std=c11

#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h>

#include <libvq.h>

void print_usage(const char* progname);

int main(int argc, char *argv[]){
	/*
	CMDLINE PARSING
	*/
	int c;
	int ndims = 0;		// Number of dimensions (defaults to 2, but inits in 0 for practical reasons)
	int* dims = NULL;	// Dimensions length (dynamic array)
	int nunits = 2500;	// Number of net units
	int nepochs = 10;	// Number of epochs
	double nradius_ini = 0;	// Initial neighborhood radius (If 0, take nunits/2 further along)
	double nradius_fini = 0;	// Final neighborhood radius
	double lrate_ini = 1.0;
	double lrate_fini = 0.0;
	int batchflag = 0;	// Shows if either batch training (0) or sequential training (1) must be used
	int ngflag = 0;		// Shows if either SOM (0) or batch Neural Gas (1) must be used
	int supflag = 0;	// Shows if either the supervised algorithm is used (1) or not (0)
	char* ifilename = "input.csv";
	char* ofilename = "output.csv";
	while((c = getopt(argc,argv,"i:o:L:e:n:bNsh")) != -1){
		switch(c){
		case 'i':
			ifilename = optarg;
			break;
		case 'o':
			ofilename = optarg;
			break;
		case 'L':
			ndims++;
			if (ngflag && ndims>1){
				puts("Too many -L flags. Neural gas doesn't have a topology");
				return -1;
			}
			int dimsize = atoi(optarg);
			if (dimsize<1){
				puts("Invalid parameter for the -L flag");
				return -1;
			}
			dims = realloc(dims,ndims*sizeof *dims);
			if (!dims){
				//We don't really need to free the memory on exit.
				//The OS won't let us leak it anyway.
				puts("Error allocating memory parsing the -L flag");
				return -1;
			}
			dims[ndims-1] = dimsize;
			if (ndims == 1)
				nunits = dimsize;
			else
				nunits *= dims[ndims-1];
			break;
		case 'e':
			nepochs = atoi(optarg);
			if (nepochs<0){ // Allow 0 epochs to output the initialized untrained net
				puts("Invalid parameter for the -e flag");
				return -1;
			}
			break;
		case 'n':
			nradius_ini = atoi(optarg);
			if (nradius_ini<1){
				puts("Invalid parameter for the -n flag");
				return -1;
			}
			break;
		case 'b':
			batchflag = 1;
			break;
		case 'N':
			ngflag = 1;
			break;
		case 's':
			supflag = 1;
			break;
		case 'h':
		default:
			print_usage(argv[0]);
			return 0;
		}
	}

	/*
	Fix the default values if needed
	*/
	if(ngflag){
		if (!batchflag){
			puts("Sequential Neural Gas is not implemented. Add the -b flag");
			return -1;
		}
		else if (!ndims){ // Default NG
			dims  = malloc(sizeof *dims);
			dims[0] = nunits;
		}
		else
			ndims = 0; //ndims==0 is NG, dims is then asumed to have length 1
	}
	else if (!ndims){ // Default SOM
		ndims = 2;
		dims  = malloc(2*sizeof *dims);
		if (!dims)
			return -1;
		dims[0] = 50;
		dims[1] = 50;
		nunits = dims[0]*dims[1];
	}
	if (!nradius_ini){
		if (ngflag)
			nradius_ini = (double)nunits/2.0;
		else
			nradius_ini = dims[0];
	}
	if (!nradius_fini){
		if (ngflag)
			nradius_fini = 0.01;
		else
			nradius_fini = 1;
	}

	/*
	DATA LOADING AND MEMORY ALLOCATIONS
	*/
	struct libvq_dataset* data = libvq_dataset_init(ifilename, supflag);
	if (!data){
		fputs("\n",stderr);
		return -1;
	}
	libvq_normalizedata(data);

	struct libvq_net* vq = libvq_create_net(ndims,dims,data);
	if (!vq){
		fputs("Error initializing the VQ net\n",stderr);
		return -1;
	}

	#ifdef VQEXPLORER_DETERMINISTIC_INIT
	libvq_initialize_net(vq);
	#else
	libvq_randomize_net(vq);
	#endif

	if (batchflag){
		printf("Starting Batch %s training\n", ngflag?"Neural Gas":"SOM");
		libvq_batchtrain(vq,nepochs,nradius_ini,nradius_fini);
	}
	else{
		printf("Starting Sequential %s training\n", ngflag?"Neural Gas":"SOM");
		libvq_seqtrain(vq,nepochs,lrate_ini,lrate_fini,nradius_ini,nradius_fini);
	}
	puts("Done.");

	/*
	QUANTIZATION ERROR
	*/
	printf("Quantization error: %f\n", libvq_getquantizationerror(vq));
	if (supflag)
		printf("Reconstruction error: %f\n", libvq_getreconstructionerror(vq));

	/*
	WRITE .CSV OUTPUT
	*/
	#ifndef VQEXPLORER_NORMALIZED_OUTPUT
	libvq_denormalize_net(vq);
	#endif
	libvq_csvwrite_net(vq, ofilename);

	/*
	FREE MEMORY
	*/
	libvq_net_fini(vq);
	libvq_dataset_fini(data);
	free(dims);
	return 0;
}

void print_usage(const char* progname){
	printf("Usage: %s [options]\n", progname);
	puts("Load and process CSV files. By default reads \"input.csv\" and writes to\n"
		"\"output.csv\", but that behaviour can be overriden using the optional\n"
		"arguments in [options].\n\n"
		"Optional arguments:\n"
		"  -i\tInput file\n"
		"  -o\tOutput file\n"
		"  -L\tLength of a single dimension\n"
		"  -e\tNumber of epochs\n"
		"  -n\tInitial neighborhood radius\n"
		"  -b\tEnable batch training\n"
		"  -N\tUse Batch Neural Gas instead of SOM\n"
		"  -s\tUse a supervised version of the chosen algorithm\n"
		"\nExamples:\n");
	printf("    %s -i~/foo.csv -o~/bar.csv\n",progname);
	puts("        Input  <- ~/foo.csv\n        Output -> ~/bar.csv");
	printf("    %s -ofoobar.csv\n",progname);
	puts("        Input  <- ./input.csv\n        Output -> ./foobar.csv");
	printf("    %s -e200\n",progname);
	puts("        Epochs <- 200");
	printf("    %s -L43 -L32 -L10\n",progname);
	puts("        Map Size <-43x32x10");
}
