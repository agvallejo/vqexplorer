#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <csvparser.h>
#include "libvq_priv.h"

struct libvq_dataset* libvq_dataset_init(const char* filename, int supflag){
	/*
	IMPORTANT: Though in any sensible environment NULL has all bits set
	as zero. The C standard doesn't mandate so. It's therefore possible
	that this little calloc trick to set the pointers to NULL may not work
	on some rare machines. If such case ever happens... well, change it.

	There is an analogous trick in libvq_create_net(...)
	*/
	struct libvq_dataset* data = calloc(1,sizeof *data);
	if (!data)
		return NULL;

	/*
	CSV parser instantiation
	*/
	CsvParser *csvparser = CsvParser_new(filename,",",1);
	CsvRow *header;
	CsvRow *row;

	/*
	CSV Header dump
	*/
	header = CsvParser_getHeader(csvparser);
	if (!header){
		fputs(CsvParser_getErrorMessage(csvparser),stderr);
		CsvParser_destroy(csvparser);
		return NULL;
	}
	const char **headerFields = CsvParser_getFields(header);
	data->nvars = CsvParser_getNumFields(header);
	if (supflag) //If this is a supervised algorithm we have nvars==nfields-1
		data->nvars--;

	data->varnames = malloc(data->nvars*sizeof *(data->varnames));
	data->mean = malloc(data->nvars*sizeof *(data->mean));
	data->std_dev = malloc(data->nvars*sizeof *(data->std_dev));
	if (!data->varnames || !data->mean || !data->std_dev){
		CsvParser_destroy(csvparser);
		libvq_dataset_fini(data);
		return NULL;
	}

	/*
	Nullify each element so that in case of error ahead we can simply
	call dataset_fini(...) to do the clean-up (we can't free non alloc'd
	memory, but free(NULL) is innocuous)
	*/ 
	for (int i=0; i<data->nvars; i++)
		data->varnames[i] = NULL;

	/*
	Store the labels
	*/
	for (int i=0; i<data->nvars; i++){
		data->varnames[i]=malloc(strlen(headerFields[i])+1); //Account for the '\0' character!
		if (!data->varnames[i]){
			CsvParser_destroy(csvparser);
			libvq_dataset_fini(data);
			return NULL;
		}
		strcpy(data->varnames[i], headerFields[i]);
	}
	if (supflag){
		data->sup.name = malloc(strlen(headerFields[data->nvars])+1);
		if (!data->sup.name){
			libvq_dataset_fini(data);
			return NULL;
		}
		strcpy(data->sup.name, headerFields[data->nvars]);
	}

	/*
	CSV Data dump
	*/
	data->nsamples = 0;
	while ((row = CsvParser_getRow(csvparser))){
		data->nsamples++;
		// We realloc each iteration to account for the extra sample, and we use
		// a temporary 'tmp' variable in order to not lose the pointer to the old
		// memory in case of error (else we might end up in a memory leak)
		double* tmp = realloc(data->matrix, data->nsamples*data->nvars*sizeof *tmp);

		if (!tmp){
			libvq_dataset_fini(data);
			CsvParser_destroy_row(row);
			CsvParser_destroy(csvparser);
			return NULL;
		}
		data->matrix = tmp;
		const char **rowFields = CsvParser_getFields(row);
		for (int i=0; i<data->nvars; i++)
			data->matrix[(data->nsamples-1)*data->nvars+i]=atof(rowFields[i]);

		if (supflag){ // If supervised, the last column is actually the target
			tmp = realloc(data->sup.target, data->nsamples*sizeof *tmp);
			if (!tmp){
				libvq_dataset_fini(data);
				CsvParser_destroy_row(row);
				CsvParser_destroy(csvparser);
				return NULL;
			}
			data->sup.target = tmp;
			data->sup.target[data->nsamples-1]=atof(rowFields[data->nvars]);
		}

		CsvParser_destroy_row(row);
	}

	// And kill the parser. At this point CSVparser should be gone
	// and every single bit of memory is used and managed by libvq.
	CsvParser_destroy(csvparser);
	return data;
}

void libvq_dataset_fini(struct libvq_dataset* const data){
	// It is assumed that every pointer points either to NULL
	// or a valid allocated memory region in order to be able
	// to always call safely dataset_fini(...)
	free(data->matrix);
	if (data->varnames)
		for (int i=0; i<data->nvars; i++)
			free(data->varnames[i]); //No need no nullify
	free(data->varnames);
	free(data->mean);
	free(data->std_dev);
	free(data->sup.target);
	free(data->sup.name);
	free(data);
}

void libvq_normalizedata(struct libvq_dataset* const data){
	/*
	Calculate the means
	*/
        // This first loop initializes the means, just in case they weren't
	for (int i=0; i<data->nvars; i++)
		data->mean[i] = data->matrix[i]/data->nsamples;

	for (int i=1; i<data->nsamples; i++)
		for (int j=0; j<data->nvars; j++)
			data->mean[j] += data->matrix[data->nvars*i+j]/data->nsamples;

        if (data->sup.target){
                data->sup.mean = data->sup.target[0]/data->nsamples;
                for (int i=1; i<data->nsamples; i++)
                        data->sup.mean += data->sup.target[i]/data->nsamples;
        }

	/*
	Calculate the standard deviations
	---
	NOTE: Bessel's correction is used in the estimate of the variance 
	for consistency with MATLAB. Note that such estimator is unbiased.
	*/
	if(data->nsamples==1){
		for (int i=0; i<data->nvars; i++)
			data->std_dev[i] = 1;
		if (data->sup.target)
			data->sup.std_dev = 1;
	}
	else{
		for (int i=0; i<data->nvars; i++)
			data->std_dev[i] = pow(data->matrix[i]-data->mean[i],2);

		for (int i=1; i<data->nsamples; i++)
			for (int j=0; j<data->nvars; j++)
				data->std_dev[j] += pow(data->matrix[data->nvars*i+j]-data->mean[j],2);

		for (int i=0; i<data->nvars; i++){
			data->std_dev[i] = sqrt(data->std_dev[i]/(data->nsamples-1));
			if (data->std_dev[i] == 0)
				data->std_dev[i] = 1;
		}

		if (data->sup.target){
			data->sup.std_dev = pow(data->sup.target[0]-data->sup.mean,2);
			for (int i=1; i<data->nsamples; i++)
				data->sup.std_dev += pow(data->sup.target[i]-data->sup.mean,2);
			data->sup.std_dev = sqrt(data->sup.std_dev/(data->nsamples-1));
			if (data->sup.std_dev == 0)
				data->sup.std_dev = 1;
		}
	}

	/*
	Normalize the data
	*/
	for (int i=0; i<data->nsamples; i++)
		for (int j=0; j<data->nvars; j++)
			data->matrix[i*data->nvars+j] = (data->matrix[i*data->nvars+j]-data->mean[j])/data->std_dev[j];

        if (data->sup.target)
		for (int i=0; i<data->nsamples; i++)
			data->sup.target[i] = (data->sup.target[i]-data->sup.mean)/data->sup.std_dev;
}
