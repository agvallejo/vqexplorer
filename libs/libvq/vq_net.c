#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <assert.h>

#include "libvq_priv.h"

struct libvq_net* libvq_create_net(int ndims, int* dims, struct libvq_dataset* const data){
	/*
	Gracefully manage erroneous parameters
	*/
	if (ndims<0)
		return NULL;

        /*
        IMPORTANT: Though in any sensible environment NULL has all bits set
        as zero. The C standard doesn't mandate so. It's therefore possible
        that this little calloc trick to set the pointers to NULL may not work
        on some rare machines. If such case ever happens... well, change it.
	
	There is an anologous trick in libvq_dataset_init(...)
        */
	struct libvq_net* vq = calloc(1,sizeof *vq);
	if (!vq)
		return NULL;

	/*
	Start filling
	*/
	vq->data = data;
	vq->top.ndims = ndims;

	/*
	With this allocation the responsabilities of caller and callee are
	obvious. If dynamic, the caller frees the parameter dims, and libvq
	frees its own copy in libvq_net_fini(...)
	*/
	vq->top.dims = malloc(ndims*sizeof *(vq->top.dims));
	if (!vq->top.dims){
		libvq_net_fini(vq);
		return NULL;
	}
	for (int i=0; i<ndims; i++)
		vq->top.dims[i]=dims[i];

	/*
	Get the number of neurons
	Note that for ndims == 0 there is still one element in *dims
	for the number of units of Neural Gas, which doesn't have a
	topology
	*/
	if (ndims>0){
		vq->nunits = 1;
		for (int i=0; i<vq->top.ndims; i++){
			if (vq->top.dims[i]<1){
				libvq_net_fini(vq);
				return NULL;
			}
	
			vq->nunits *= vq->top.dims[i];
		}
	}
	else
		vq->nunits = dims[0];

	/*
	Dynamically create the net of the appropriate size
	*/
	vq->net = malloc(vq->data->nvars*vq->nunits*sizeof *(vq->net));
	if (!vq->net){
		libvq_net_fini(vq);
		return NULL;
	}

	/*
	Dynamically create the net of gradients and targets of the appropriate size (if supervised)
	*/
	if (data->sup.target){
		vq->target   = malloc(vq->nunits*sizeof *(vq->target));
		vq->grad_net = malloc(vq->data->nvars*vq->nunits*sizeof *(vq->grad_net));
		if (!vq->grad_net || !vq->target){
			libvq_net_fini(vq);
			return NULL;
		}
	}

/*
FIXME: This shouldn't really be here, but in som_train.c
*/
	if (vq->top.ndims>0){ //If SOM
		// Dynamically create the distance array of the appropriate size (N^2)
		vq->top.dists = malloc(pow(vq->nunits,2)*sizeof *(vq->top.dists));
		if (!vq->top.dists){
			libvq_net_fini(vq);
			return NULL;
		}

		//Brute-forcefully initialize the dists array
		for (int i=0; i<vq->nunits; i++)
			for (int j=0; j<vq->nunits; j++)
				vq->top.dists[i*vq->nunits+j] = getkerneldist(vq,i,j);
	}

	return vq;
}

/*
Deterministic initialization (only for testing purposes):
	The proper way to do this involves calculating the eigenvalues
	of the dataset, but this function is merely used to create pre-defined
	prototypes so that libVQ can be checked against a mature implementation.
	As such, the initialization chosen is just a linear increment across
	each scalar in the VQ list (simplest as it can possibly be).

	---WARNING---
	The datasets are normalized, therefore you could easily end up with initial
	values extending beyond the input domain. For few prototypes with few 
	variables everything should be ok. But beware those cases. This function
	is for testing purposes ONLY for a good reason. (Nevertheless, it should still
	work, as the training itself will return the prototypes to the input domain)
*/
void libvq_initialize_net(struct libvq_net* const vq){
	static const double delta= 0.01;
	for (int i=0; i<vq->data->nvars*vq->nunits; i++)
		vq->net[i] = delta*i;
	if (vq->target){ // If supervised, also initialize the targets
		for (int i=0; i<vq->nunits; i++){
			vq->target[i] = 10*delta*(i+1);
			for (int j=0; j<vq->data->nvars; j++)
				vq->grad_net[vq->data->nvars*i+j] = delta*(vq->data->nvars*i+j);
		}
	}
}

void libvq_randomize_net(struct libvq_net* const vq){
/*
  NOTE: RAND() && SRAND() ARE NOT THREAD-SAFE. 
	----------------------------------------
	Due to the minor use rand() is given here it probably won't
	ever give any problems, but if the library gets thread support
	and several instances of this function get called concurrently
	the internal state of srand() will give every thread the same
	random map even if each thread calls srand() with a different 
	seed. The randomized maps (otherwise perfectly valid) MIGHT
	or MIGHT NOT be important for the particular application.

	Given the high amount of IFs, ANDs and MIGHTs in the above note
	I won't even consider making this function thread-safe.
*/

	//srand() should only be called ONCE
	static int used_seed = 0;
	if (!used_seed){
		srand(time(NULL));
		used_seed++;
	}

	//Randomly initialize with null mean, unit variance
	for (int i=0; i<vq->data->nvars*vq->nunits; i++)
		vq->net[i]=12*(rand()/(double)RAND_MAX-0.5);
	if (vq->target){ // If supervised, also randomize the targets
		for (int i=0; i<vq->nunits; i++){
			vq->target[i]=12*(rand()/(double)RAND_MAX-0.5);
			vq->grad_net[i]=12*(rand()/(double)RAND_MAX-0.5);
		}
	}
}

void libvq_net_fini(struct libvq_net* vq){
	// Don't free vq->data, as it was expicitly allocated elsewhere
	// and it should be explicitly free'd by libvq_dataset_fini(...) there
	free(vq->net);
	free(vq->target);
	free(vq->grad_net);
	free(vq->top.dists);
	free(vq);
}

void libvq_denormalize_net(struct libvq_net* const vq){
	for (int i=0; i<vq->nunits; i++)
		for (int j=0; j<vq->data->nvars; j++)
			vq->net[i*vq->data->nvars+j] = vq->net[i*vq->data->nvars+j]*vq->data->std_dev[j]+vq->data->mean[j];
	if (vq->target) // If supervised
		for (int i=0; i<vq->nunits; i++)
			vq->target[i] = vq->target[i]*vq->data->sup.std_dev+vq->data->sup.mean;
			
}
