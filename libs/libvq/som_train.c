#include <stdio.h>

#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "libvq_priv.h"

void som_batchtrain(struct libvq_net* const vq, int nepochs, double nradius_ini, double nradius_fini){
	// 'bmuhit[i]' is the number of samples which have the i-th neuron as their bmu
	int* bmuhit = malloc(vq->nunits*sizeof *bmuhit);

	// 'voronoi_sum' is the sum of the samples in the Voronoi region of each neuron times the kernel
	double* voronoi_sum = malloc(vq->nunits*vq->data->nvars*sizeof *voronoi_sum);

	// 'num' will hold the independent numerators of each variable
	double* num = malloc(vq->data->nvars*sizeof *num);

	// We are using a linear progression in the neighborhood radius
	double nradius		= nradius_ini;
	double nradius_step	= 0;
	if (nepochs>1) //We want to prevent dividing by zero
		nradius_step	= (nradius_ini-nradius_fini)/(nepochs-1);

	#if defined(LIBSOM_PARALLEL) && defined(LIBSOM_DEBUG)
	puts("MULTI-THREADING ENABLED");
	#endif

	for (int n=0; n<nepochs; n++){
		#ifdef LIBSOM_DEBUG
		printf("Epoch %d\n",n+1);
		#endif
		memset(bmuhit,0,vq->nunits*sizeof *bmuhit);
		memset(voronoi_sum,0.0,vq->nunits*vq->data->nvars*sizeof *voronoi_sum);
		#ifdef LIBSOM_PARALLEL
		#pragma omp parallel for
		#endif
		for (int i=0; i<vq->data->nsamples; i++){
			// BMU calculation
			int bmu = libvq_getbmu(vq->data->matrix+i*vq->data->nvars,vq);
			#ifdef LIBSOM_PARALLEL
			#pragma omp critical
			#endif
			{
				bmuhit[bmu]++;
				// N*mean(x) calculation
				for (int j=0; j<vq->data->nvars; j++)
					voronoi_sum[bmu*vq->data->nvars+j] += vq->data->matrix[i*vq->data->nvars+j];
			}
		}

		double kden = 2*pow(nradius,2);
		for (int i=0; i<vq->nunits;i++){
			memset(num,0.0,vq->data->nvars*sizeof *num);
			double den = 0.0; //Denominator
			for (int j=0; j<vq->nunits; j++){
				if (!bmuhit[j])
					continue;
				double h = exp(-vq->top.dists[i*vq->nunits+j]/kden);
				for (int k=0; k<vq->data->nvars; k++)
					num[k] += h*voronoi_sum[j*vq->data->nvars+k];
				den += h*bmuhit[j];
			}
			if (!den)
				continue;

			for (int j=0; j<vq->data->nvars; j++){
				vq->net[i*vq->data->nvars+j] = num[j]/den;
			}
		}
		nradius -= nradius_step;
	}

	free(num);
	free(voronoi_sum);
	free(bmuhit);
}

void som_seqtrain(struct libvq_net* const vq, int nepochs, double lrate_ini, double lrate_fini, double nradius_ini, double nradius_fini){
	double lrate = lrate_ini;
	double lrate_step = (lrate_ini-lrate_fini)/(vq->data->nsamples*nepochs-1);

	double nradius		= nradius_ini;
	double nradius_step	= 0;
	if (nepochs>1) //We want to prevent dividing by zero
		nradius_step	= (nradius_ini-nradius_fini)/(nepochs*vq->data->nsamples-1); 
	for (int n=0; n<nepochs; n++){
		#ifdef LIBSOM_DEBUG
		printf("Epoch %d:\n",n+1);
		#endif
		datashuffle(vq->data);
		double kden = 2*pow(nradius,2);
		for (int i=0; i<vq->data->nsamples; i++){
			int bmu = libvq_getbmu(vq->data->matrix+i*vq->data->nvars,vq);
			for (int j=0; j<vq->nunits; j++){
				double h = lrate*exp(-vq->top.dists[j*vq->nunits+bmu]/kden);
				for (int k=0; k<vq->data->nvars; k++){
					vq->net[j*vq->data->nvars+k] *= 1-h;
					vq->net[j*vq->data->nvars+k] += h*vq->data->matrix[i*vq->data->nvars+k];
				}
			}
			lrate -= lrate_step;
			nradius  -= nradius_step;
		}
	}
}
