#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

#include "libvq_priv.h"

void ng_batchtrain(struct libvq_net* const vq, int nepochs, double alpha_ini, double alpha_fini){
	/*
	'alpha': Decay factor
	'num': Holds the numerators of each variable of every 'nunit'
	'den': Holds the denominators of every 'nunit'
	'target_num': Holds the numerators of the target
	'rank': Holds the (ordered by rank) indices and distances of every 'nunit' to a given sample
	*/
	double alpha;
	double* num     = malloc(vq->data->nvars*vq->nunits*sizeof *num);
	double* den     = malloc(vq->nunits*sizeof *den);
	double* target_num = vq->target?malloc(vq->nunits*sizeof *target_num):NULL; //Only allocate in supervised
	struct ng_rank* rank= malloc(vq->nunits*sizeof *rank);
	if (!num || !den || (vq->target && !target_num) || !rank){
		free(num);
		free(den);
		free(target_num);
		free(rank);
		return;
	}
	for (int i=0; i<vq->nunits; i++)
		rank[i].index=i;

	#if defined(LIBVQ_PARALLEL) && defined(LIBVQ_DEBUG)
	puts("MULTI-THREADING ENABLED");
	#endif

	for (int n=0; n<nepochs; n++){
		/*
		EXPLANATION:
		----
		For each epoch, get a sorted list of the distances to the i-th sample
		along with each prototype's index. Then, sweep the prototypes increasing
		the rank, not the index (The index is obtained from the sorted structures
		themselves.)
		*/
		#ifdef LIBVQ_DEBUG
		printf("Epoch = %d\n",n+1);
		#endif

		memset(num,0.0,vq->data->nvars*vq->nunits*sizeof *num);
		memset(den,0.0,vq->nunits*sizeof *den);
		if (vq->target)
			memset(target_num,0.0,vq->nunits*sizeof *target_num);

		alpha = alpha_ini*pow(alpha_fini/alpha_ini,(double)n/nepochs);
		for (int i=0; i<vq->data->nsamples; i++){
			getranks(rank, vq, vq->data->matrix+i*vq->data->nvars);
			for (int j=0; j<vq->nunits; j++){
				double h = exp(-j/alpha);
				if (isnan(h))
					continue;
				den[rank[j].index] += h;
				if (vq->target)
					target_num[rank[j].index] += h*vq->data->sup.target[i];
				for (int k=0; k<vq->data->nvars; k++)
					num[rank[j].index*vq->data->nvars+k] += h*vq->data->matrix[i*vq->data->nvars+k];
			}
		}

		for (int i=0; i<vq->nunits; i++){
			if (!den[i])
				continue;
			for (int j=0; j<vq->data->nvars; j++)
				vq->net[i*vq->data->nvars+j] = num[i*vq->data->nvars+j]/den[i];
			if (vq->target)
				vq->target[i] = target_num[i]/den[i];
		}
	}

	if (vq->target){
		#ifdef LIBVQ_DEBUG
		puts("Estimating gradients...");
		#endif

		for (int n=0; n<nepochs; n++){
			#ifdef LIBVQ_DEBUG
			printf("Epoch = %d\n",n+1);
			#endif

			memset(num,0.0,vq->data->nvars*vq->nunits*sizeof *num);
			memset(den,0.0,vq->nunits*sizeof *den);

			alpha = alpha_ini*pow(alpha_fini/alpha_ini,(double)n/nepochs);
			for (int i=0; i<vq->data->nsamples; i++){
				getranks(rank, vq, vq->data->matrix+i*vq->data->nvars);
				for (int j=0; j<vq->nunits; j++){
					double h = exp(-j/alpha);
					if (isnan(h))
						continue;
					den[rank[j].index] += h*rank[j].dist;
					double dotprod = 0.0;
					for (int k=0; k<vq->data->nvars; k++){
						dotprod += vq->grad_net[rank[j].index*vq->data->nvars+k]*\
								(vq->data->matrix[i*vq->data->nvars+k] - vq->net[rank[j].index*vq->data->nvars+k]);
					}
					for (int k=0; k<vq->data->nvars; k++){
						num[rank[j].index*vq->data->nvars+k] +=\
								h*\
								(vq->data->matrix[i*vq->data->nvars+k]-vq->net[rank[j].index*vq->data->nvars+k])*\
								(vq->data->sup.target[i] - (vq->target[rank[j].index] + dotprod));
					}
				}
			}
			for (int i=0; i<vq->nunits; i++){
				if (!den[i])
					continue;
				for (int j=0; j<vq->data->nvars; j++)
					vq->grad_net[i*vq->data->nvars+j] += num[i*vq->data->nvars+j]/den[i];
			}
		}
	}

	free(num);
	free(den);
	free(target_num);
	free(rank);
}
