#include <stdlib.h>
#include <math.h>

#include "libvq_priv.h"

int libvq_getbmu(const double* const data, const struct libvq_net* const vq){
	//Initially we assume the first neuron is the BMU (as good a guess as any)
	double dist = getsquareddist(vq->data->nvars, vq->net, data);
	int bmu=0;

	for (int i=1; i<vq->nunits; i++){
		double tmp_dist = getsquareddist(vq->data->nvars, vq->net+i*vq->data->nvars, data);
		if (dist>tmp_dist){
			dist = tmp_dist;
			bmu = i;
		}
	}
	return bmu;
}

double libvq_getquantizationerror(const struct libvq_net* const vq){
	double error = 0.0;
	for (int i=0; i<vq->data->nsamples; i++){
		int bmu = libvq_getbmu(vq->data->matrix+i*vq->data->nvars,vq);
		error +=sqrt(getsquareddist(vq->data->nvars,
				vq->data->matrix+i*vq->data->nvars,
				vq->net+bmu*vq->data->nvars));
	}
	return error/vq->data->nsamples;
}

double libvq_getestimate(const struct libvq_net* const vq, double* data){
	int bmu = libvq_getbmu(data,vq);
	double result = 0.0;
	for (int i=0; i<vq->data->nvars; i++)
		result += vq->grad_net[bmu*vq->data->nvars+i]*(data[i]-vq->net[bmu*vq->data->nvars+i]);
	result += vq->target[bmu];
	return result;
}

double libvq_getreconstructionerror(const struct libvq_net* const vq){
	double error = 0.0;
	for (int i=0; i<vq->data->nsamples; i++){
		error += pow(vq->data->sup.target[i]-\
				libvq_getestimate(vq, vq->data->matrix+i*vq->data->nvars)\
				,2);
	}
	return sqrt(error/vq->data->nsamples);
}

double getsquareddist(int nvars, const double* const src1, const double* const src2){
	double dist=0;
	for(int i = 0; i<nvars; i++){
		dist += pow(src1[i]-src2[i],2);
	}

	return dist;
}

double getkerneldist(const struct libvq_net* const vq, int index1, int index2){
	double dist = pow(index2%vq->top.dims[0] - index1%vq->top.dims[0],2);
	for (int i=1; i<vq->top.ndims; i++){
		index1 /= vq->top.dims[i-1];
		index2 /= vq->top.dims[i-1];
		dist += pow(index2%vq->top.dims[i] - index1%vq->top.dims[i],2);
	}
	return dist;
}

void datashuffle(struct libvq_dataset* const data){
	/*
	The idea is: for each sample in the dataset
	swap it with some PREVIOUSLY ENCOUNTERED sample.
	*/
	for (int i = 1; i<data->nsamples; i++){
		//Take a random index lower than 'i'
		int j = rand()%(i+1);

		//Swap 'i' and 'j'
		for (int k=0; k<data->nvars; k++){
			double tmp = data->matrix[j*data->nvars + k];
			data->matrix[j*data->nvars+k] = data->matrix[i*data->nvars + k];
			data->matrix[i*data->nvars+k] = tmp;
		}
	}
}

static int cmp(const void* ptr1, const void* ptr2){
        return ((struct ng_rank*)ptr1)->dist<((struct ng_rank*)ptr2)->dist?-1:1;
}

void getranks(struct ng_rank* const rank, const struct libvq_net* const vq, const double* const sample){
        /*
        This function deserves some disertation:
        ---
        Ok, getting the ranks is hard no matter how you do it. If you're not
        careful you might end up with an O(N^2) algorithm (ouch!). So, what's the
        best way to sort this mess? (Don't you get it? SORT the mess... :) )

        Now, HOW to sort it is up to some debate because... surprise, it (may) depend(s)
        on the datasets. I didn't go very far thinking about this, but glibc's qsort uses
        "merge sort" (As of September 2016), which has a worst-case complexity of
	O(N*logN), but doesn't order in-place (spacial complexity of O(N)).
	Other libc implementations tend to rely on quicksort (worst-case of O(N^2),
	but usually faster, with theoretical averages of O(N*logN)). Asuming the ranks
	won't vary too much (and that's asuming quite a lot) it MIGHT be a cool idea using
	Insertion Sort and preserve the ranks from call to call to end up almost always
	in O(N).

        But qsort is just fine in my tests, so I won't code my own sorting routines.
        If at some point the speed becomes critical this might be a good route.
        */
        
        // Calculate all the distances to the sample
        for (int i=0; i<vq->nunits; i++){
                rank[i].dist = 0.0;
                for (int j=0; j<vq->data->nvars; j++)
                        rank[i].dist += pow(sample[j]-vq->net[rank[i].index*vq->data->nvars+j],2);
        }

        qsort(rank,vq->nunits,sizeof *rank,cmp);
}
