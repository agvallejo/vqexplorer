#include <stdio.h>
#include <stdlib.h>

#include "libvq_priv.h"

/*
FIXME: Remove duplicate code, this two functions should be condensed into a backend and
two frontends.
*/
enum libvq_state libvq_csvwrite_data(const struct libvq_dataset* const data, const char* filename){
	FILE* fd = fopen(filename,"w");
	if (!fd)
		return FILE_ERROR;

	/*
	Print the header
	*/
	fprintf(fd,"%s",data->varnames[0]);
	for (int i=1; i<data->nvars; i++)
		fprintf(fd,",%s",data->varnames[i]);
	if (data->sup.name) // If supervised, print target
		fprintf(fd,",%s",data->sup.name);
	fprintf(fd,"\n");

	/*
	Print the data
	*/
	for (int i=0; i<data->nsamples; i++){
		fprintf(fd,"%f",data->matrix[data->nvars*i]);
		for (int j=1; j<data->nvars; j++)
			fprintf(fd,",%f",data->matrix[data->nvars*i+j]);
		if (data->sup.target) // If supervised, print target
			fprintf(fd,",%f",data->sup.target[i]);
		fprintf(fd,"\n");
	}

	fclose(fd);
	return OK;
}

enum libvq_state libvq_csvwrite_net(const struct libvq_net* const vq, const char* filename){
	FILE* fd = fopen(filename,"w");
	if (!fd)
		return FILE_ERROR;

	/*
	Print the header
	*/
	if (vq->top.ndims){ // If SOM, also print the grid positions
		fprintf(fd,"x1");
		for (int i=1; i<vq->top.ndims; i++)
			fprintf(fd,",x%d",i+1);
		fprintf(fd,",");
	}
	fprintf(fd,"%s",vq->data->varnames[0]);
	for (int i=1; i<vq->data->nvars; i++){
		fprintf(fd,",%s",vq->data->varnames[i]);
	}
	if (vq->target){ // If supervised, print target and gradients
		fprintf(fd,",%s",vq->data->sup.name);
		for (int i=0; i<vq->data->nvars; i++)
			fprintf(fd,",grad_%s",vq->data->varnames[i]);
	}
	fprintf(fd,"\n");

	/*
	Calculate offsets of the grid positions to properly go back to zero
	*/
	int* offset = NULL;
	if (vq->top.ndims){ //If SOM
		offset = malloc(vq->top.ndims*sizeof *offset);
		if (!offset){
			fclose(fd);
			return MALLOC_ERROR;
		}
		offset[0] = vq->top.dims[0];
		for (int i=1; i<vq->top.ndims; i++)
			offset[i]=vq->top.dims[i]*offset[i-1];
	}

	/*
	Print the data
	*/
	for (int i=0; i<vq->nunits; i++){
		if (vq->top.ndims){ // If SOM, print the grid
			fprintf(fd,"%d",i%vq->top.dims[0]);
			for (int j=1; j<vq->top.ndims; j++){
				fprintf(fd,",%d",(i/offset[j-1])%vq->top.dims[j]);
			}
			fprintf(fd,",");
		}

		fprintf(fd,"%f",vq->net[vq->data->nvars*i]);
		for (int j=1; j<vq->data->nvars; j++)
			fprintf(fd,",%f",vq->net[vq->data->nvars*i+j]);

		if (vq->target){ // If supervised, print target and gradients
			fprintf(fd,",%f",vq->target[i]);
			for (int j=0; j<vq->data->nvars; j++)
				fprintf(fd,",%f",vq->grad_net[vq->data->nvars*i+j]);
		}
		fprintf(fd,"\n");
	}

	free(offset);
	fclose(fd);
	return OK;
}
