#ifndef LIBVQ_PRIV_H
#define LIBVQ_PRIV_H

#include <libvq.h>

/*
DATA STRUCTURES
*/
struct sup_data{
	double* target;			// Flattened targets (supervised algorithm)
	char* name;			// Label of the target (supervised algorithm)
	double mean;			// Unscaled mean of each variable
	double std_dev;			// Variance of the targets
};

struct libvq_dataset{
	// Generic fields
	double* matrix; 		// Flattened data (matrix in a vector)
	char** varnames;		// Labels
	int nvars;			// Number of variables in the dataset
	int nsamples;			// Number of samples in the dataset
	double* mean;			// Unscaled mean of each variable
	double* std_dev;		// Variance of each variable

	// Only used in supervised algorithms
	struct sup_data	sup;		// (if 'supervised', holds info about the targets)
};

struct som_topology{
	int ndims;			// Number of dimensions of the SOM map
	int* dims;			// Array with the length of each dimension
	double* dists;			// Array with the inter-neuron distances
};

struct libvq_net{
	struct libvq_dataset* data;		// Dataset used during training
	double* net;			// Flattened net
	int nunits;			// Number of net units

	// Only used in the SOM algorithm
	struct som_topology top;	// (if 'type'==SOM), information regarding the topology

	// Only used in supervised algorithms
	double* target;			// Flattened targets (for linear supervised models)
	double* grad_net;		// Flattened gradients net (for linear supervised models)
};

struct ng_rank{
        int index;
        double dist;
};

/*
Training
*/
void som_seqtrain(struct libvq_net* const vq, int epochs, double lrate_ini, double lrate_fini, double nradius_ini, double nradius_fini);
void som_batchtrain(struct libvq_net* const vq, int epochs, double nradius_ini, double nradius_fini);
void ng_batchtrain(struct libvq_net* const vq, int epochs, double alpha_ini, double alpha_fini);

/*
Auxiliary functions
*/
double getsquareddist(int nvars, const double* const src1, const double* const src2);
double getkerneldist(const struct libvq_net* const vq, int index1, int index2);
void datashuffle(struct libvq_dataset* const data);
void getranks(struct ng_rank* const rank, const struct libvq_net* const vq, const double* const sample);

#endif
