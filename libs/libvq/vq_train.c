#include <stdio.h>

#include "libvq_priv.h"

void libvq_seqtrain(struct libvq_net* const vq, int nepochs, double lrate_ini, double lrate_fini, double nradius_ini, double nradius_fini){
	if (!vq->top.ndims){ // If using NG
		puts("Sequential Neural Gas not implemented");
		return;
	}
	som_seqtrain(vq, nepochs, lrate_ini, lrate_fini, nradius_ini, nradius_fini);
}

void libvq_batchtrain(struct libvq_net* const vq, int nepochs, double decay_ini, double decay_fini){
	// decay is a generic name for nradius and alpha
	if (vq->top.ndims)
		som_batchtrain(vq, nepochs, decay_ini, decay_fini);
	else
		ng_batchtrain(vq, nepochs, decay_ini, decay_fini);
}
