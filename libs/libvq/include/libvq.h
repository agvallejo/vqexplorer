#ifndef LIBVQ_H
#define LIBVQ_H

struct libvq_dataset;
struct libvq_net;

enum libvq_state{
	OK,
	FILE_ERROR,
	MALLOC_ERROR,
	PARAMS_ERROR
};

/*
PROTOTYPES FROM DATASET.C
*/
struct libvq_dataset* libvq_dataset_init(const char* filename, int supflag);
void libvq_dataset_fini(struct libvq_dataset* const data);
void libvq_normalizedata(struct libvq_dataset* const data);

/*
PROTOTYPES FROM VQ_NET.C
*/
struct libvq_net* libvq_create_net(int ndims, int* dims, struct libvq_dataset* const data);
void libvq_initialize_net(struct libvq_net* const vq);
void libvq_randomize_net(struct libvq_net* const vq);
void libvq_net_fini(struct libvq_net* vq);
void libvq_denormalize_net(struct libvq_net* const vq);

/*
PROTOTYPES FROM VQ_TRAIN.C
*/
void libvq_seqtrain(struct libvq_net* const vq, int epochs, double lrate_ini, double lrate_fini, double nradius_ini, double nradius_fini);
void libvq_batchtrain(struct libvq_net* const vq, int epochs, double decay_ini, double decay_fini);

/*
PROTOTYPES FROM VQ_AUX.C
*/
int libvq_getbmu(const double* const data, const struct libvq_net* const vq);
double libvq_getquantizationerror(const struct libvq_net* const vq);
double libvq_getestimate(const struct libvq_net* const vq, double* data);
double libvq_getreconstructionerror(const struct libvq_net* const vq);

/*
PROTOTYPES FROM CSVWRITE.C
*/
enum libvq_state libvq_csvwrite_data(const struct libvq_dataset* const vq, const char* filename);
enum libvq_state libvq_csvwrite_net(const struct libvq_net* const vq, const char* filename);

#endif
