CC := clang
AR := ar
MAKE := make

CFLAGS ?= -O3 -std=c11 -Wall -Wextra -pedantic -MMD -MP
CPPFLAGS ?= -Ilibs/libvq/include -Ilibs/libcsvparser/include -Iinclude
LIBS := -Llibs -lvq -lcsvparser -lm

include include.mk/files.mk
include include.mk/config.mk
include include.mk/targets.mk

#FIXME: Something needs to be implemented to  rebuild in case of a change in compiler flags (e.g: make vs make DEBUG=y)

%.o: %.c
	@$(CC) $(CFLAGS) $(CPPFLAGS) -c $< -o $@

-include $(DEPFILES)
