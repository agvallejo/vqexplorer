# The Build System
Throughout the article I emphasize when I'm talking about GNU products. Far from political or philosophical reasons, I do so because GNU Make is **very** different from BSD Make. So much that making use of advanced features while mantaining cross-compatibility is close to impossible. That said, if you think you have a way of keeping the build scheme while letting BSD Make into the game I'd be glad to hear it.

## Overview
VQexplorer is built by a non-recursive GNU Make scheme (see Peter Miller's famous paper [Recursive Make Considered Harmful](http://aegis.sourceforge.net/auug97.pdf) for a little insight on the problems of incomplete DAGs and the potential pitfalls of recursive Make).

The files shown below are the files accessed by Make in order to build VQexplorer:

```
.
├── include.mk
│   ├── config.mk
│   ├── files.mk
│   └── targets.mk
.
.
.
└── Makefile
```

* ```Makefile```: It's the entry point to the Make program, where some general compilation flags may be set or unset.
* ```config.mk```: Here configuration passed to Make may be processed (e.g: ```make RELEASE=y``` disables text information during training to improve performance)
* ```files.mk```: Where every file related to the project is listed with the ```find``` command so that ```targets.mk``` knows what files to compile.
* ```targets.mk```: In this file every phony target is defined. They instruct Make in what should be built and/or rebuilt.

## Drawbacks
On smaller programs this design might seem like an overkill, while in even bigger ones it might lack flexibility. Either way, this is an example of functional non-recursive Make scripts. For a more flexible (though admittedly more complex) one based on the same principle, you may take a look [here](https://bitbucket.org/pdclib/pdclib) or [here](https://bitbucket.org/agvallejo/stradivari).

## How to build
### Requirements
A minimal POSIX environment is required. That's easy enough to obtain on a Linux or BSD machine, but getting it to work on Windows requires some more work (Cygwin might be a good candidate, and is known to work as of October 2016).

As for the software part, you'll need (obviously) a compiler&linker (both Clang and GCC were extensively tested), an archiver (only the ar from GNU binutils was tested, but BSD ar should work as well) and a Make implementation (sadly, it must be GNU Make, as explained in the Overview)

### Typical usage
In order to build the software you can safely use the default target. It should compile the libraries and the main program, and link them with debug messages and single-thread mode

```
make
```

For a more specialized build you may specify any of the flags listed in ```include.mk/config.mk```. For instance:
```
make PARALLEL=y
make RELEASE=y
```

They can also be used at the same time, as shown below:
```
make PARALLEL=y RELEASE=y

```

### Insight into the black magic of the compilation process
It's far from obvious how Make determines the interdependencies of the files, as part of the process depends on the compiler itself and the ```.d``` files it generates. Perhaps the best explanation found online is [this one](http://wiki.osdev.org/Makefile), highly based on the build system used by the ~~[PDCLib project](https://bitbucket.org/pdclib/pdclib)~~ (apparently, not anymore. The old mantainer, Solar, left and whoever took over the project changed the build system. In any case this paragraph is still valid and the first link still explains the Make approach).

Roughly, the idea is that (assuming there's only a single translation unit per ```.c``` file, as it most certainly should be) the compiler will generate a ```.d``` dependency file for each ```.c``` file, and that file will be populated with Make's syntax to be imported in the next iteration. This way, not only their associated ```.h``` files will be checked (e.g: ```foo.h``` is a dependency of ```foo.c```, but so is ```bar.h``` , but the whole DAG. As I said before, [Peter Miller's paper](ttp://aegis.sourceforge.net/auug97.pdf) provides extensive discussion on the matter.

## Beyond VQexplorer
VQexplorer is a good enough for most offline purposes, but it's too general for certain kinds of applications. If VQexplorer simply doesn't cut it, the alternative is to create a custom software on top of libVQ. There are several ways to achieve this, and below are the two typical scenarios you might face:

### Replace ```main.c```
The easiest by far. Simply replace the main file with your own and recompile. Though simple this approach may fall short in large applications with multiple libraries and complex build schemes. It's best suited to simple CLI interfaces requiring only a few translation units (aside from the libraries). The built-in Make scripts will browse the source tree and build everything.

Say you want to create a ```SOMoverSockets``` software that somehow uses internet sockets to do it's thing or whatever VQexplorer can't do. In order to integrate everything together, you have a certain ```main.c```, which calls functions from ```mySocketAPI.c``` and ```prettyMessages.c``` through the header ```coolHeader.h```. For this particular example, the building scripts provided with VQexplorer are more than enough. Just copy ```coolHeader.h``` into the ```include``` folder on the root of the project and the ```.c``` files on the root itself. Then include the headers as:

```
#!c

#include <coolHeader.h>
#include <libvq.h>
```

or equivalently:

```
#!c

#include "include/coolHeader.h"
#include <libvq.h>
```
However, whereas the former scales well with multiple nested projects and complex build rules, the latter does not. Therefore, as a rule of thumb use angle brackets along with the ```-I``` flag except when you want to hide information (as is the case with ```libvq_priv.h```, which shouldn't be used outside the library itself, for example).

### Integrate the library into another project
Depending on how good the scripts are this task might be actually easier or way (WAY) harder.

In order to truly understand how to do this and not get it wrong it's important to know [how libVQ is organized](https://bitbucket.org/agvallejo/vqexplorer/wiki/The%20libVQ%20Organization), at least on a very basic level.

Each project is free to decide to do things their way. And the worse part of it is that there are thousands of ways of CORRECTLY building a program. It's impossible to predict how they will structure their scripts, or if they expect you to modify directly CFLAGS or through another environment variables.

The only hint I can give you is to consult their documentation on how they expect you to import libraries.

Ideally, they'll have some sort of default ```include``` folder, where you can leave ```libvq.h``` and some sort of ```libs``` folder where you may leave either the source code of the library or the compiled version of it. Specifics on how to do that are simply outside the scope of this text.

Then, using the ```-I```, ```-L``` and ```-l``` flags you should be able to correctly instruct the compiler link against the library using the specified include dirs. If you are to use those flags directly or some other way also remain outside of the scope of the text.
