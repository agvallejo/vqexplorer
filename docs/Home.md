# Welcome

This wiki serves as the knowledge base for vqexplorer, and libVQ in particular

## libVQ features

libVQ is an extensible [Vector Quantization](https://en.wikipedia.org/wiki/Vector_quantization) library, currently implementing [Self-Organizing Maps](https://en.wikipedia.org/wiki/Self-organizing_map) (SOM), and [Neural Gas](https://en.wikipedia.org/wiki/Neural_gas) (NG). The library is written exclusively in C11 (so C89 compilers won't be able to compile and some newer compilers might need special flags) in order to ease the creation of language bindings in the near future.

* [The Build System](https://bitbucket.org/agvallejo/vqexplorer/wiki/The%20Build%20System)
* [The libVQ API](https://bitbucket.org/agvallejo/vqexplorer/wiki/The%20libVQ%20API)
* [The libVQ organization](https://bitbucket.org/agvallejo/vqexplorer/wiki/The%20libVQ%20Organization)
* [VQexplorer](https://bitbucket.org/agvallejo/vqexplorer/wiki/VQexplorer)
