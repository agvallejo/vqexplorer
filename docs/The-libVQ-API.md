# The libVQ API
The API can be viewed through the main header. Thus this is a commented ```libvq.h```.
## Opaque types
```
#!c
struct libvq_dataset;
struct libvq_net;
```
The main data structures. They are created and modified through the libVQ API. A "dataset" holds information on a certain block of data (the raw data itself, as well as some metadata, such as variables names, targets, etc).

Analogously, a "net" holds information on the raw data of the quantized net, as well as information on the output topology (in the case of SOM training). It also holds information on the input dataset, so that you only need a reference to the "net" for a training. By doing so, libVQ can guarantee internal sanity (As it will only create valid nets, with compatible datasets. e.g: Same number of variables, targets, etc.)

## Return codes
```
#!c
enum libvq_state{
        OK,
        FILE_ERROR,
        MALLOC_ERROR,
        PARAMS_ERROR
};
```
Return codes for the CSV writter. There's a long road ahead in terms of error notification, and most errors go unnoticed for the rest of the library. Taking the current design into account, the proper way to handle this problems would be through perror and similar standard features of C. But they are at the moment unimplemented.
 
## Prototypes
```
#!c
/*
PROTOTYPES FROM DATASET.C
*/
struct libvq_dataset* libvq_dataset_init(const char* filename, int supflag);
void libvq_dataset_fini(struct libvq_dataset* const data);
void libvq_normalizedata(struct libvq_dataset* const data);

/*
PROTOTYPES FROM VQ_NET.C
*/
struct libvq_net* libvq_create_net(int ndims, int* dims, struct libvq_dataset* const data);
void libvq_randomize_net(struct libvq_net* const vq);
void libvq_net_fini(struct libvq_net* vq);
void libvq_denormalize_net(struct libvq_net* const vq);

/*
PROTOTYPES FROM VQ_TRAIN.C
*/
void libvq_seqtrain(struct libvq_net* const vq, int epochs, double lrate_ini, double lrate_fini, double nradius_ini, double nradius_fini);
void libvq_batchtrain(struct libvq_net* const vq, int epochs, double decay_ini, double decay_fini);

/*
PROTOTYPES FROM VQ_AUX.C
*/
int libvq_getbmu(const double* const data, const struct libvq_net* const vq);
double libvq_getquantizationerror(const struct libvq_net* const vq);
double libvq_getestimate(const struct libvq_net* const vq, double* data);
double libvq_getreconstructionerror(const struct libvq_net* const vq);

/*
PROTOTYPES FROM CSVWRITE.C
*/
enum libvq_state libvq_csvwrite(const struct libvq_net* const vq, const char* filename);
```
This is the whole API of libVQ as of October 2016. It's not final and subject to changes, but the main paradigm is in place.

1. Create a dataset.
2. Normalize it.
3. Create a discretized net.
4. Choose topology (and therefore training algorithm).
5. Choose training variant (batch vs sequantial).
6. (opt) Use the auxiliary functions to evaluate the results.
