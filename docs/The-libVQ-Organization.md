# The libVQ Organization

libVQ is actually a simple library. Even though it's split in several translation units, the access to it lies in a single public header ```libvq.h```. Note that there is a private header ```libvq_priv.h``` not included in the compiler's search path. That's on purpose and it shouldn't be used outside the library itself.

The build scripts will analyze the library folder and add any translation unit you crate to the library. As of October 2016 the main translation units are a CSV writer (```csvwrite.c```), data structure related routines (creation, destruction, normalization...) in ```dataset.c``` and ```vq_net.c```, one translation unit per algorithm (```ng_train.c``` and ```som_train.c```) and one entry point to the algorithms in ```vq_train.c```.

All of them include the private header ```libvq_priv.h```, which in turn includes ```libvq.h```.

## Important warning on opaque structs
```libvq.h``` was crafted in order to ensure that the relevant data structures (libvq_dataset and libvq_net) would be opaque. That way, libVQ can be sure that it's the only responsible part in mantaining the data structures' integrity. I might not be clear enough, so let me repeat that.

**Don't access the hidden members of the structs.**

And in particular:

**Don't assume libVQ will check for consistency in transparent structs.**

And if bold letters aren't suggestive enough, I also have a few formal reasons why the data structures should be opaque.

* If libVQ is used as a shared library, the use of opaque structs allows to change the internal representation of them without recompiling the blob it links against. That's specially useful when you consider that the library will most likely be simpler than the interface that gives access to it. Think about GUI with wxWidgets, GTK or QT, for example. They are massive.

* libVQ can always assume sanity conditions, which leads to lower execution times. That's not particularly useful or important with the current API, but in the cases where that very API has to be used iteratively in a loop, having sanity checks every iteration may drag down performance dramatically.

## How to extend libVQ
The main reason you may be tempted to access the internals of the structs outside of libVQ (**don't**) is to do some exotic processing (maybe a sequence of trainings or some non-linear transformation of the nets). If that's the case simply add a new translation unit to the library (```myfeature.c```) that include the private header and does whatever you need it to do. Then, add the prototypes of your additions to the API to ```libvq.h``` and call them from the main blob. Ideally the added features should be general enough to be added to the library, but for a specific application this approach is perfectly ok.
