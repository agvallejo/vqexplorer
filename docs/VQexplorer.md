# VQexplorer
VQexplorer is a simple CLI to ease the use of libVQ in typical vector quantization applications. The IO is carried out in CSV formats, as libVQ doesn't support any other format at the moment (due to lack of interest, it's trivial to add more formats).

## Typical use
You can get a list of parameters to the program by adding the help flag.

```
#!c
vqexplorer -h
```

```
#!none
Load and process CSV files. By default reads "input.csv" and writes to
"output.csv", but that behaviour can be overriden using the optional
arguments in [options].

Optional arguments:
  -i	Input file
  -o	Output file
  -L	Length of a single dimension
  -e	Number of epochs
  -n	Initial neighborhood radius
  -b	Enable batch training
  -N	Use Batch Neural Gas instead of SOM
  -s	Use a supervised version of the chosen algorithm

Examples:

    ./vqexplorer -i~/foo.csv -o~/bar.csv
        Input  <- ~/foo.csv
        Output -> ~/bar.csv
    ./vqexplorer -ofoobar.csv
        Input  <- ./input.csv
        Output -> ./foobar.csv
    ./vqexplorer -e200
        Epochs <- 200
    ./vqexplorer -L43 -L32 -L10
        Map Size <-43x32x10
```
As of October 2016, this is the output of the help. In order to use any of the available flags you have to call the program along with each flag immediately followed by the parameter it takes. For example:

* Train a 2D SOM with a grid of 10x23 prototypes through 200 epochs with the dataset in ```/home/myuser/datasets/mynotsocoolinput.csv```, and leave the output in ```/home/myuser/maps/mycoolmap.csv```. Use batch training for efficiency. The initial neighborhood radius should be [42](https://simple.wikipedia.org/wiki/42_(answer))

```
vqexplorer -L10 -L23 -e200 -i/home/myuser/datasets/mynotsocoolinput.csv -o/home/myuser/maps/mycoolmap.csv -b -n42
```

The order of the flags is not important, but it is important to know the default values.

```
2D SOM (50x50)
epochs=10
sequential training
input=./input.csv
output=./output.csv
neighborhood radius=decay rate=length of the first dimension or half the number of units in neural gas

```
This way you can include only the different flags and not all of them

## Interpretation of the CSV files
* No NaN or empty values are allowed.
* The variable separator is the comma.
* The first row of the file is a header containing the label names of each variable.
* For output files, the first N variables account for the N dimensions of the output topology and are indicated as ```x1, x2, ..., xN```.
* For supervised mode, the last variable in the input file is the target. As such that an input file with a header ```y1, y2, y3, z``` is actually a 3-variables input space with a 1-variable target.
* For supervised mode, in the output file the header follows this convention ```<N topology variables>, <M data variables>, target, <M gradient variables>```. The gradients (necessary for estimation) follow the pattern ```grad_<variable name>```.

### Examples

Input file:
```
pressure, size, volume, efficiency
1,3,2,0.9
2,2,1,0.8
2,5,1,0.3
```

Output file (unsupervised, 2D SOM [2x3])
```
x1,x2,pressure, size, volume, efficiency
0,0,1.338218,2.937362,1.661782,0.820248
1,0,1.532528,2.662754,1.467472,0.814200
0,1,1.666635,3.333292,1.333365,0.666682
1,1,1.786663,2.853173,1.213337,0.714694
0,2,1.908131,4.077005,1.091869,0.478331
1,2,1.934895,3.467271,1.065105,0.572816
```
Output file (supervised, 2D SOM [2x3]. Note that the target variable doesn't have a gradient)
```
x1,x2,pressure, size, volume, efficiency,grad_pressure,grad_ size,grad_ volume
0,0,1.920742,3.125505,1.079258,-1.523587,-0.862090,-2.742309,2.608412
1,0,1.950054,3.820365,1.049946,3.102525,2.849665,3.452607,0.667555
0,1,1.616905,3.084458,1.383095,0.447213,0.000000,0.000000,0.000000
1,1,1.725115,3.625527,1.274885,1.219437,0.000000,0.000000,0.000000
0,2,1.183202,3.025288,1.816798,-0.447207,0.000000,0.000000,0.000000
1,2,1.269698,3.232132,1.730302,-2.816301,0.000000,0.000000,0.000000
```

### Visualization
VQexplorer doesn't include a builtin visualization solution (it wasn't designed to). However a ```.tex``` script is bundled with the source code to read the output ```.csv``` and make beautiful graphics with them to import in your documents. It's not automatic and requires manual tuning to read the proper variables. it uses variables x1 and x2 to determine the grid positions and created a map of each variable. Use with care, as big maps require good amounts of RAM and possible manual tuning the maximum allowed memory usage of the pdfTeX interpreter.
