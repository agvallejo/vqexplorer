# Enable the debug symbol unless we are on Release
ifneq ($(RELEASE),y)
	CPPFLAGS := $(CPPFLAGS) -DLIBVQ_DEBUG
endif

# Disable multi-threading unless asked
ifeq ($(PARALLEL),y)
	CFLAGS := $(CFLAGS) -fopenmp
	CPPFLAGS := $(CPPFLAGS) -DLIBVQ_PARALLEL
endif

# Enable random initialization unless asked to have a deterministic net
ifeq ($(DETERMINISTIC),y)
	CPPFLAGS := $(CPPFLAGS) -DVQEXPLORER_DETERMINISTIC_INIT
endif

# Enable denormalization prior to writeback unless asked
ifeq ($(NORMALIZED_OUTPUT),y)
	CPPFLAGS := $(CPPFLAGS) -DVQEXPLORER_NORMALIZED_OUTPUT
endif
