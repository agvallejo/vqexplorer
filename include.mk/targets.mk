.PHONY: clean all run valgrind todolist distrib

all: vqexplorer

vqexplorer: libs/libvq.a libs/libcsvparser.a $(MAIN_OBJFILES)
	@echo -n "Linking vqexplorer... "
	@$(CC) $(CFLAGS) $(CPPFLAGS) $(MAIN_OBJFILES) -o $@ $(LIBS)
	@echo Done.

libs/libvq.a: $(LIBVQ_OBJFILES)
	@echo -n "Generating libvq.a... "
	@$(AR) rcs $@ $(LIBVQ_OBJFILES)
	@echo Done.

libs/libcsvparser.a: $(LIBCSVPARSER_OBJFILES)
	@echo -n "Generating libcsvparser.a... "
	@$(AR) rcs $@ $(LIBCSVPARSER_OBJFILES)
	@echo Done.
clean:
	@echo -n 'Cleaning... '
	@rm -rf $(OBJFILES) $(DEPFILES) vqexplorer libs/libcsvparser.a libs/libvq.a gmon.out
	@echo Done.

run: vqexplorer
	@./vqexplorer -iwork/input.csv -owork/output.csv -b $(EXTRAFLAG)

run_ng: vqexplorer
	@./vqexplorer -iwork/input.csv -owork/output.csv -N -b $(EXTRAFLAG)

run_ngs: vqexplorer
	@./vqexplorer -iwork/input.csv -owork/output.csv -N -b -s $(EXTRAFLAG)

show: run
	@cd work && pdflatex plot.tex && evince plot.pdf

valgrind: vqexplorer
	@valgrind -v --leak-check=full --show-leak-kinds=all --track-origins=yes ./vqexplorer -iwork/input.csv -owork/output.csv -N -b -e1 -L10 $(EXTRAFLAG)

prof: run
	@gprof ./vqexplorer gmon.out | vim -

prof_ng: run_ng
	@gprof ./vqexplorer gmon.out | vim -

todolist:
	-@for file in $(ALLDEVFILES:Makefile=); do fgrep -H -e TODO -e FIXME $$file; done;    true

distrib:
	@$(MAKE) clean
	@tar czf ../libvq.tar.gz .
