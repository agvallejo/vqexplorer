# Load every .c file except for those in the libs directory
# and their related object files
MAIN_SRCFILES := $(shell find . -type f -name "*.c" -not -path "./libs/*" -not -path "./docs/*")
MAIN_OBJFILES := $(MAIN_SRCFILES:.c=.o)

# Load every .c file in the libcsvparser directory
# and their related object files
LIBCSVPARSER_SRCFILES := $(shell find libs/libcsvparser -type f -name "*.c")
LIBCSVPARSER_OBJFILES := $(LIBCSVPARSER_SRCFILES:.c=.o)

# Load every .c file in the libvq directory
# and their related object files
LIBVQ_SRCFILES := $(shell find libs/libvq -type f -name "*.c")
LIBVQ_OBJFILES := $(LIBVQ_SRCFILES:.c=.o)

# Combined source, object and dependency files (WITHOUT headers)
SRCFILES := $(MAIN_SRCFILES) $(LIBCSVPARSER_SRCFILES) $(LIBVQ_SRCFILES)
OBJFILES := $(SRCFILES:.c=.o)
DEPFILES := $(SRCFILES:.c=.d)

# Combined source, object and dependency files (WITH headers)
ALLDEVFILES := $(SRCFILES) $(shell find . -type f -name "*.h")
